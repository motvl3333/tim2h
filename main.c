// in the PsyQ TECHREF CD: Devrefs/Filefrmt.pdf contains the documentation
// of TIM files

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// number of bytes before CLUT/PIXEL entries begin:
// bnum (4 bytes),
// DX   (2 bytes),
// DY   (2 bytes),
// W    (2 bytes),
// H    (2 bytes)
#define BLOCK_METADATA_SIZE 12 

#define VERSION_MAJOR 0
#define VERSION_MINOR 5

//////////////// prototypes ////////////////

// the values of bnum and entries are set from the values read from TIM file
void read_block_metadata(FILE *tim, unsigned int *bnum, unsigned int *entries);

// "type" string is only used to help with naming conventions when writing to the file
void write_block_data_to_c_header(FILE *tim, FILE *output_file, 
      char *output_filename, char *type, unsigned int entries);

void display_help();

//////////////// variables ////////////////
FILE *tim, *output, *output2;
char *input_filename;          // will either point to argv[1] or argv[2]. should look something like this: xxxx.tim
char output_filename[256];     // pure filename (only used when NOT exporting to a raw file (i.e. -f is NOT passed) )
char output_filename_ex[256];  // pure filename + extension (either "example.h" or "example.clt")
char output_filename_ex2[256]; // pure filename + extension 2 ("example.pxl")
unsigned char PMODE, CF;
unsigned int clut_bnum, pixel_bnum;
unsigned int clut_entries, pixel_entries;

char export_to_raw_file; // turned on if -f option is passed

//////////////// main ////////////////
int main(int argc, char **argv)
{
  export_to_raw_file = 0;

  //////////////// process arguments ////////////////

  if(argc == 1){
    display_help();
    return 0;
  }

  else if(argc > 3){
    fprintf(stderr, "Too many arguments!\nTry typing tim2h for more information.\n");
    return 1;
  }

  // process options and decide whether or not input_filename should be argv[1] or argv[2].
  else if(argc == 2 || argc == 3){

    // use str to access individual letters of argv[1]
    char *str = argv[1];
    
    // if the first letter(s) of the first argument to the program starts with "--" or "-", the user passed an option
    // check for "--" and "-" by checking characters individually, for they should not be null-terminated strings 

    // if long option is passed
    if( strlen(str) > 2 && (str[0] == '-' && str[1] == '-') ){

      // help
      if( !strcmp(&str[2], "help") ){
        display_help();
        return 0;
      }

      // file
      else if( !strcmp(&str[2], "file") ){

        export_to_raw_file = 1;

        // needs one more argument
        if(argc == 2){
          fprintf(stderr, "The option you've provided requires another argument.\n");
          return 1;
        }

        // use third argument as input filename
        else if(argc == 3){
          input_filename = argv[2];
        }
      }

      // invalid option
      else{
        fprintf(stderr, "The option you've provided is invalid.\nTry typing tim2h for more information.\n");
        return 1;
      }
    }

    // short option is passed (pretty much the same as above)
    else if( strlen(str) > 1 && str[0] == '-' ){

      // help
      if( !strcmp(&str[1], "h") ){
        display_help();
        return 0;
      }

      // file
      else if( !strcmp(&str[1], "f") ){

        export_to_raw_file = 1;

        // needs one more argument
        if(argc == 2){
          fprintf(stderr, "The option you've provided requires another argument.\n");
          return 1;
        }

        // use third argument as input filename
        else if(argc == 3){
          input_filename = argv[2];
        }
      }

      // invalid option
      else{
        fprintf(stderr, "The option you've provided is invalid.\nTry typing tim2h for more information.\n");
        return 1;
      }
    }

    // no option was passed, so use argv[1] as the input file
    else{

      input_filename = argv[1];
    }
  }


  // make sure input file is long enough
  if(strlen(input_filename) <= 4){
    fprintf(stderr, "Invalid input filename!\nHint: append \".tim\" to your file: %s.tim\n", input_filename);
    return 1;
  }

  // make sure input file is not too long (max characters being 255 leaves the 256th character to be '\0')
  if(strlen(input_filename) > 255){
    fprintf(stderr, "The input and output filenames must be less than 256 characters (including the .tim extension).\n");
    return 1;
  }

  // check if the input file ends in ".tim"
  {
    // tim_extension represents where the ".tim" should be in the filename index-wise
    unsigned int tim_extension = strlen(input_filename) - 4; 

    // check if .tim extension is missing from filename
    if( strcmp( &input_filename[tim_extension], ".tim" ) != 0 ){
      fprintf(stderr, "Invalid input file!\nHint: append \".tim\" to your file: %s.tim", input_filename);
      return 1;
    }
  }

  tim = fopen(input_filename, "rb");

  if(!tim){
    fprintf(stderr, "Unable to open file!\n");
    return 1;
  }

  {
    // length of input_filename without ".tim" extension
    unsigned int length = strlen(input_filename) - 4;

    // export raw CLUT and PIXEL data to separate files with .clt and .pxl extensions respectively
    if(export_to_raw_file){
  
      // create name CLUT file
      strncpy(output_filename_ex, input_filename, length);
      output_filename_ex[length] = '\0'; // the string must be null-terminated before using strcat()

      strcat(output_filename_ex, ".clt");

      // create name of PIXEL file
      strncpy(output_filename_ex2, input_filename, length);
      output_filename_ex2[length] = '\0';

      strcat(output_filename_ex2, ".pxl");

      printf("\nCreating %s..\n", output_filename_ex);
      printf("Creating %s..\n", output_filename_ex2);

      // now open them in binary mode. binary mode has no effect on POSIX systems, but on Windows, it
      // disables special handling of the '\n' and '\x1a' characters
      output = fopen(output_filename_ex, "wb");
      output2 = fopen(output_filename_ex2, "wb");
    }
  
    // export CLUT and PIXEL data to separate arrays in a C header file
    else{
  
      // output_filename is used later (when naming arrays within the file)
      strncpy(output_filename, input_filename, length);
      output_filename[length] = '\0';
  
      // generate output filestream
      // output_filename_ex is only used here as an argument to fopen()
      strncpy(output_filename_ex, input_filename, length);
      output_filename_ex[length] = '\0';
  
      strcat(output_filename_ex, ".h");
  
      printf("\nCreating %s..\n", output_filename_ex);
  
      output = fopen(output_filename_ex, "wb");
    }

  }

  // now it's time to read the TIM file

  // skip ID block
  fseek(tim, 4, SEEK_SET);

  // read FLAG block
  {
    unsigned char buffer;

    fread(&buffer, 1, 1, tim);

    PMODE = (buffer & 1<<0) + \
            (buffer & 1<<1) + \
            (buffer & 1<<2);

    CF = (buffer & 1<<3) ? 1 : 0;

    // NO:  fseek(tim, SEEK_CUR, 3);
    // YES: fseek(tim, 3, SEEK_CUR);
    fseek(tim, 3, SEEK_CUR);
  }

  // only supports 4bpp, 8bpp, 16bpp. does not support
  // 24-bit or mixed CLUTs
  if(PMODE > 2){

    // PMODE == 3: 24-bit direct
    // PMODE == 4: Mixed
    if(PMODE == 3 || PMODE == 4){
      fprintf(stderr, "This program only supports 4bpp, 8bpp, and 16bpp images.\n");
      exit(1);
    }
    
    else if(PMODE > 4 || CF > 2){
      fprintf(stderr, "The TIM image you have given is corrupted!\n");
      exit(2);
    }
    
    else{
      fprintf(stderr, "Unknown error!\n");
      exit(1);
    }

  }

  // read CLUT block
  clut_bnum = 0;
  clut_entries = 0;

  if(CF){
    printf("\nCLUT\n~~~~\n");

    read_block_metadata(tim, &clut_bnum, &clut_entries);

    // write the CLUT data from the TIM image into the raw ".clt" file
    // a 8bpp image has a CLUT with 256 entries, each entry being 2 bytes large resulting in 512 bytes
    if(export_to_raw_file){

      unsigned char buffer[clut_entries*2];

      fread(buffer, 2, clut_entries, tim);
      fwrite(buffer, 2, clut_entries, output);

    }

    // write CLUT data in the form of an array to a C header file
    else{
      write_block_data_to_c_header(tim, output, output_filename, "clut", clut_entries);
    }
  }

  // read PIXEL block
  pixel_bnum = 0;
  pixel_entries = 0;

  printf("\nPIXEL\n~~~~~\n");

  read_block_metadata(tim, &pixel_bnum, &pixel_entries);

  // write the PIXEL data from the TIM image into the raw ".pxl" file
  // the max size for a 16bpp image is 256x256px, resulting in 131072 bytes
  if(export_to_raw_file){
    
    unsigned char buffer[pixel_entries*2];

    fread(buffer, 2, pixel_entries, tim);
    fwrite(buffer, 2, pixel_entries, output2);
  }

  // write PIXEL data in the form of an array to a C header file
  else{
    write_block_data_to_c_header(tim, output, output_filename, "pixel", pixel_entries);
  }

  printf("\nData written successfully!\n");

  fclose(output);

  if(export_to_raw_file){
    fclose(output2);
  }
  
  fclose(tim);

  return 0;
}

// we set our values of bnum and entries from the TIM file in this function
void read_block_metadata(FILE *tim, unsigned int *bnum, unsigned int *entries){

  // MAKE SURE THAT THEY ARE FUCKING UNSIGNED buffers for reading data
  unsigned char int_buffer[4];   // int == 4 bytes
  unsigned char short_buffer[2]; // short == 2 bytes

  // read all block metadata
  unsigned short DX = 0, DY = 0, W = 0, H = 0;
  
  // debug
  unsigned int bytes_read_successfully = 0;

  *bnum = 0; *entries = 0;

  // read bnum block from block metadata
  for(int i = 0; i < 4; i++){
    
    if( (int_buffer[i] = fgetc(tim)) == EOF){
      fprintf(stderr, "Error reading TIM block metadata.\n\n%d/%d bytes read.\n\n", bytes_read_successfully, BLOCK_METADATA_SIZE);
      exit(1);
    }

    bytes_read_successfully++;

    // ensure proper bit positions of integer for each byte
    *bnum += (int_buffer[i] << (i*8) );
  }

  // repeat for DX, DY, W, and H
  
  /////////////// DX ///////////////
  for(int i = 0; i < 2; i++){

    // if fgetc() returns EOF, there was an error
    if( (short_buffer[i] = fgetc(tim)) == EOF ){
      fprintf(stderr, "Error reading TIM block metadata.\n\n%d/%d bytes read.\n\n", bytes_read_successfully, BLOCK_METADATA_SIZE);
      exit(1);
    }

    bytes_read_successfully++;

    DX += short_buffer[i] << (i*8);
  }

  /////////////// DY ///////////////
  for(int i = 0; i < 2; i++){

    // if fgetc() returns EOF, there was an error.. you get the idea
    if( (short_buffer[i] = fgetc(tim)) == EOF ){
      fprintf(stderr, "Error reading TIM block metadata.\n\n%d/%d bytes read.\n\n", bytes_read_successfully, BLOCK_METADATA_SIZE);
      exit(1);
    }

    bytes_read_successfully++;

    DY += short_buffer[i] << (i*8);
  }

  /////////////// W ///////////////
  for(int i = 0; i < 2; i++){

    
    if( (short_buffer[i] = fgetc(tim)) == EOF ){
      fprintf(stderr, "Error reading TIM block metadata.\n\n%d/%d bytes read.\n\n", bytes_read_successfully, BLOCK_METADATA_SIZE);
      exit(1);
    }

    bytes_read_successfully++;

    W += short_buffer[i] << (i*8);
  }
  
  /////////////// H ///////////////
  for(int i = 0; i < 2; i++){

    if( (short_buffer[i] = fgetc(tim)) == EOF ){
      fprintf(stderr, "Error reading TIM block metadata.\n\n%d/%d bytes read.\n\n", bytes_read_successfully, BLOCK_METADATA_SIZE);
      exit(1);
    }

    bytes_read_successfully++;

    H += short_buffer[i] << (i*8);
  }

  printf("bnum: %d\n", *bnum);
  printf("DX: %d\n", DX);
  printf("DY: %d\n", DY);
  printf("W: %d\n", W);
  printf("H: %d\n\n", H);
  
  printf("%d/%d bytes read successfully from TIM block metadata\n", bytes_read_successfully, BLOCK_METADATA_SIZE);

  // each entry within this block will be a short
  *entries = (*bnum - BLOCK_METADATA_SIZE)/2;
}

void write_block_data_to_c_header(FILE *tim, FILE *output_file, 
      char *output_filename, char *type, unsigned int entries)
{

  // buffer for reading data (AGAIN, UNSIGNED)
  unsigned char char_buffer;

  // data that will store all CLUT/PIXEL entries
  // each entry is 2 bytes
  unsigned char data[entries*2];
  
  unsigned int bytes_read_successfully = 0;

  // store CLUT/PIXEL entries from TIM file into data buffer
  // every CLUT/PIXEL entry is a short value
  for(int i = 0; i < entries*2; i++){

    if( (char_buffer = fgetc(tim)) == EOF){
      fprintf(stderr, "Error reading TIM block data.\n\n%d/%d bytes read.\n\n", bytes_read_successfully, entries*2);
      exit(1);
    }

    bytes_read_successfully++;
    
    data[i] = char_buffer;
  }
  
  printf("%d/%d bytes (%d entries) read successfully from TIM block data\n", bytes_read_successfully, entries*2, entries);

  fprintf(output_file, "unsigned int %s_size = %d;\n", type, entries*2);
  fprintf(output_file, "unsigned char %s_%s[] = {\n", output_filename, type);

  // now write the entries from the CLUT buffer into the file
  for(int i = 0; i < entries*2; i++){
    
    // 15 entries per row
    if( !(i%15) && i != 0){
      fprintf(output_file, "\n");
    }

    // write the current entry from the CLUT/PIXEL buffer properly
    // if this iteration isn't the final iteration, append comma
    if(i != entries*2 - 1){

      if(data[i] < 0x10){
        fprintf(output, "0x0%x,", data[i]);
      }
      else{
        fprintf(output, "0x%x,", data[i]);
      }
    }

    // if it IS the final iteration, do not append the comma and add
    // ending curly brace to array in the file
    else{
      if(data[i] < 0x10){
        fprintf(output, "0x0%x", data[i]);
      }
      else{
        fprintf(output, "0x%x", data[i]);
      }

      fprintf(output, "\n};\n");
    }
  }
}

void display_help()
{
  printf("Usage: tim2h [OPTION] [FILE]\n");
  printf("version %d.%d\n\n", VERSION_MAJOR, VERSION_MINOR);
  printf("Extracts the CLUT and PIXEL data from a TIM file and stores it into a C header file. Works for TIM images that are 4bpp, 8bpp, or 16bpp.\n\n");
  printf("  -f, --file        extract the CLUT and PIXEL data into raw data files. automatically append .CLT and .PXL as extensions\n");
  printf("  -h, --help        display this help page\n\n");
  printf("Exit codes:\n");
  printf(" 0 -OK\n");
  printf(" 1 -Minor issue\n");
  printf(" 2 -File corrupted\n");
}

